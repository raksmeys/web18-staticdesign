// create function no parameter
function greetinMessage(){
    console.log("Welcome to function")
}
// call function
greetinMessage()

// function with parameter
function welcomeMessage(name = "Meychhing"){
    console.log(`Welcome, ${name}`)
}
welcomeMessage("Nika")
welcomeMessage()

// function return
// function sum(num1, num2){
//     let result  = num1 + num2
//     return result;
// }
// console.log(sum(10, 20));
// let sumResult = sum(10, 20)
// console.log(sumResult)

//arrow function, just shorthand function
// () => {}
let greet = () => console.log("Welcome, arrow function")
// syntax to execute arrow function: call name of variable()


let sum = (num1, num2) => {
    return num1 + num2
}
console.log(sum(40, 60))

let button = document.querySelector("#btn")
button.addEventListener("click", function(){
    console.log("button was clicked")
    greet()
})
button.addEventListener('click', () => {
    greet()
})
