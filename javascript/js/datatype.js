// Data Type
// 1. String: use to store text
let fullName = "Bora"
let lastName = 'Dara'
// string interpolation, template literal
let greetingMSG = `Welcome, ${fullName} and ${lastName}, to Web Class`
console.log(greetingMSG)
// 2. Number, use to store every number like integer, floating, decimal
let numberOfApples = 80
let age = 30
let score = 90.5
let yearlySalary = 3e5
let bossSalary = 5_000_500_980_879
let textAsNum = "30"
console.log(`
    age: ${age}, 
    score: ${score}, yearlySalary: ${yearlySalary},bossSalary: ${bossSalary}
`)
console.log(`
    type of age: ${typeof(age)}, type of score: ${typeof(score)}, 
    type of yearlySalary: ${typeof(yearlySalary)}
`)
// apply with operator
console.log(`
    divide by 0: ${age/0},
    string divide by text as number: ${textAsNum/age}
`)

// 4. boolean, store only true or false
let isIllegal = false
let isAdult = false
console.log(isAdult + isIllegal * 5)

// conditional statement in JS
if (isIllegal) {
    // if condition true, if block will execute
    console.log("yes i am illegal")
}else{
    console.log("else block worked")
}

if (0.1 * 3 == 0.3){
    console.log("yes it is equal")
}else{
    console.log("no, it is not")
}
// 5. undefined
let a;
console.log(a)
// 6. null
let person = null
// 7. Object

let students = {
    name: "Satya",
    age: 24,
    hobby: "Play game",
    dob: new Date("3/3/1998") // mm-dd-yyyy
}
console.log(students.age, students.name, students.hobby, students.dob)

// I want to clear student object to null
students = null
console.log(students)

// Exercise on Data Type

// use DOM, write (object) into Webpage 

let product = {
    id: 33,
    title: "iMac Book 2024",
    price: 20000,
    description: "Elevate your home office with our Modern Minimalist Workstation Setup, featuring a sleek wooden desk topped with an elegant computer, stylish adjustable wooden desk lamp, and complimentary accessories for a clean, productive workspace. This setup is perfect for professionals seeking a contemporary look that combines functionality with design.",
    images: "https://eduport.webestica.com/assets/images/courses/4by3/07.jpg",   
}

let card = `
    <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
    <a href="#">
        <img class="rounded-t-lg" src=${product.images} alt="" />
    </a>
    <div class="p-5">
        <a href="#">
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">${product.title}</h5>
        </a>
        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 line-clamp-2">${product.description}</p>
        <a href="#" class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
            Read more
            <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
            </svg>
        </a>
    </div>
    </div>
`
let section = document.querySelector("#products")
section.innerHTML = card



