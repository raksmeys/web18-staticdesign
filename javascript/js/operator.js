// Operator
// 1. Assignment Operator
let apples = 2
let oranges = 3
if (apples = oranges){
    console.log(apples)
}else{
    console.log("apple not is equal orange")
}

// Arimethic Operator +, -, *, /, %, **, ++, --
let output = apples ** oranges
let output1 = apples % oranges
let output2 = apples / oranges
console.log(output, output1, output2)
apples++
apples--
console.log(apples) // apples now is store 2 value
let numberOfApples = apples++
console.log(numberOfApples)
console.log(apples)

//Arimethic Assignment Opeator +=, -=, *=, /=, %=, **=
apples += oranges // apples = apples + oranges
apples -= oranges // apples = apples - oranges
apples ** oranges // apples = apples**oranges
console.log('after using += operator: ', apples)

// comparison operator ( ==, !=, ===, >=, >, <, <=)
let x = 10;
let y = "10"; // store number as text
if(x == y){
    console.log("Yes it is equal only value")
}
if(x === y){
    console.log("No, x not equal y")
}
// let num1 = prompt("Enter your number 1")
// let num2 = prompt("Enter your number 2")
// // console.log('you enter: ', num1, typeof(num1))
// let result = parseFloat(num1) + parseFloat(num2)
// console.log(result)

// Logical Operator ( &&, ||, !)

let person = {
    age: 20,
    gender: "male",
    name: "Nika"
}

// &&: all conditions need to be true
// ||: only one true condition >> return true
if (person.age >= 18 && person.gender == "female"){
    console.log("This person is allow to worked")
}else{
    console.log("This person is not allow to worked")
}

// String Operator: +=
let greetinMessage = "Welcome, "
greetinMessage += person.name
console.log(greetinMessage)

// exercise on string operators
let product = {
    id: 33,
    title: "iMac Book 2024",
    price: 20000,
    description: "Elevate your home office with our Modern Minimalist Workstation Setup, featuring a sleek wooden desk topped with an elegant computer, stylish adjustable wooden desk lamp, and complimentary accessories for a clean, productive workspace. This setup is perfect for professionals seeking a contemporary look that combines functionality with design.",
    images: "https://eduport.webestica.com/assets/images/courses/4by3/07.jpg",   
}

let card = `
    <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
    <a href="#">
        <img class="rounded-t-lg" src=${product.images} alt="" />
    </a>
    <div class="p-5">
        <a href="#">
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">${product.title}</h5>
        </a>
        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 line-clamp-2">${product.description}</p>
        <a href="#" class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
            Read more
            <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
            </svg>
        </a>
    </div>
    </div>
`

// Ternary Operator 
// condition ? expression_1 : expression_2
let score = 50
let message = score >= 50 ? "Passed" : "Failed"
console.log(message)

// let a = 3
// let result = (a >= 0) ? (a == 0) ? 'Zero': 'Positive' : 'Negative'
//    console.log(`The number is ${result}.`)

const a = { duration: 50 };
a.duration ??= 10;
console.log(a.duration);  // Expected output: 50

a.speed ??= 25;
console.log(a.speed);  // Expected output: 25

console.log(a)

// ?? 
const foo = null ?? 'default string';
console.log(foo); // Expected output: "default string"

const baz = 0 ?? 42;
console.log(baz);  // Expected output: 0

// array is special variable can store multiple item []


let courses = [
    {
        "title": "Web Design",
        "description": "Learn from basic to professional",
        "price": 400,
        "thumbnail": "https://eduport.webestica.com/assets/images/courses/4by3/11.jpg"
    },
    {
        "title": "Mobile App",
        "description": "Learn from basic to professional",
        "price": 400,
        "thumbnail": "https://eduport.webestica.com/assets/images/courses/4by3/05.jpg"
    },
    {
        "title": "Web Design",
        "description": "Learn from basic to professional",
        "price": 400,
        "thumbnail": "https://eduport.webestica.com/assets/images/courses/4by3/11.jpg"
    },
    {
        "title": "Mobile App",
        "description": "Learn from basic to professional",
        "price": 400,
        "thumbnail": "https://eduport.webestica.com/assets/images/courses/4by3/05.jpg"
    },
    {
        "title": "Web Design",
        "description": "Learn from basic to professional",
        "price": 400,
        "thumbnail": "https://eduport.webestica.com/assets/images/courses/4by3/11.jpg"
    },
    {
        "title": "Mobile App",
        "description": "Learn from basic to professional",
        "price": 400,
        "thumbnail": "https://eduport.webestica.com/assets/images/courses/4by3/05.jpg"
    }
]


let section = document.querySelector("#courses")

// loop in JS

// for(i = 0; i<8; i++){
//     section.innerHTML += card
// }

// special method for array
courses.map(course => {
    // section.innerHTML += card
    console.log(course)
    section.innerHTML += `
        <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
        <a href="#">
            <img class="rounded-t-lg" src=${course.thumbnail} alt="" />
        </a>
        <div class="p-5">
            <a href="#">
                <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">${course.title}</h5>
            </a>
            <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 line-clamp-2">${course.description}</p>
            <a href="#" class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                Read more
                <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
                </svg>
            </a>
        </div>
        </div>
    
    `
})

console.clear()

// Array 
// -- Create Array
let arr = new Array() // create empty array using constructor
let arr1 = [] // create empty array using literal 

let animal = new Array("cat", "dog", "pig")
let teacher = ["reksmey", "chhaya", "dara"] 
console.log(animal)
console.log(teacher)

// methods, properties
console.log(teacher.length)

teacher.unshift("Satya") // add first item to array
console.log(teacher)
teacher.push("Nika", "Visal") // add to last of array
console.log(teacher)
// 1. copy array
let teacherCopy = [...teacher]
console.log(teacherCopy)
// 2. copy array
let teacherCopy2 = Array.from(teacher)
console.log(teacherCopy2)
// 3. copy array
let teacherCopy3 = teacher.slice(3, 4)
console.log(teacherCopy3)

// pop(): remove last element
// shift(): remove first element
// splice: remove multple item
teacherCopy2.splice(0, 2, "Meychhing", "Dara2", "Dara1")

console.clear()
console.log(teacherCopy2)
// using map
let sectionTeacher = document.querySelector("#teachers")
teacherCopy2.map(function(item){
    // body function >> that performce task 
    console.log(item)
    sectionTeacher.innerHTML += `
    <div class="max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
        <a href="#">
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">${item}</h5>
        </a>
        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">${item}</p>
    </div>
  `
})

